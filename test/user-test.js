var assert = require('assert');

const db = require("../src/db/db.js");
const UserModel = require('../src/models/User.js');

const User = require("../src/actions/user.js");

const CollectionUsers = "users";

const user01 = "user01"

var username01 = "";

beforeEach(function() {
    // DELETE DB
});

// Create User
describe('User Creation', function() {
    it('Create User', function() {
	return new Promise(function(resolve) {
	    User.create(user01, user01).then(function(value){
		assert.equal(value.message, 'USER_CREATED');
		username01 = value.username;
		resolve();
	    });
	});
    });
    it('Create same User', function() {
	return new Promise(function(resolve, reject) {
	    User.create(user01, user01).then(function(lol){
		User.create(user01, user01).then(function(value){
		    console.log("not good");
		    reject();
		}).catch(function(err){		    
		    console.log(err);
		    assert.equal(err, "Error: Error: USER_FOUND");	
		    resolve();
		});
	    });
	});
    });
});
