const redis = require("redis");

const client = redis.createClient("6379", "redis");
const subscriber = redis.createClient("6379", "redis");

// Print redis errors to the console
client.on("error", err => {
  console.log(`Error ${err}`);
});

module.exports.client = client;
module.exports.subscriber = subscriber;
