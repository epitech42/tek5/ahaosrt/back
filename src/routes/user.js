// Import Node Modules
const { check, validationResult } = require("express-validator/check");

// Import Actions
const User = require("../actions/user.js");

// Import Misc Actions
const Timer = require("../actions/timer.js");
const log = require("../actions/notifications.js");

let start = new Date();

/* ROUTES */
const registerRoute = "/user/register";
const loginRoute = "/user/login";
const destroyRoute = "/user/delete";
const getUserRoute = "/user/get";
const editUserRoute = "/user/edit";

module.exports = function(app, cors) {
  /**
   * @swagger
   * /user/register:
   *   post:
   *     tags:
   *       - user
   *     description: Creates a new user
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: user
   *         description: user to create
   *         in: body
   *         required: true
   *         schema:
   *           $ref: '#/definitions/user'
   *     responses:
   *       201:
   *         description: User created
   *         schema:
   *           $ref: '#/definitions/userCreated'
   *       422:
   *         description: Error Code
   *         schema:
   *           $ref: '#/definitions/error'
   */
  app.post(
    registerRoute,
    [
      check("username").exists(),
      check("password").exists(),
      check("username", "USERNAME_NOT_VALID").custom(username => {
        console.log(/^[a-zA-Z0-9_-]*$/g.test(username));
        return /^[a-zA-Z0-9_-]*$/g.test(username);
      }),
      check("username", "USERNAME_ALREADY_EXISTING").custom(username => {
        return User.isUsernameExist(username).then(value => {
          return value;
        });
      })
    ],
    cors(),
    function(req, res) {
      start = Date.now();
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        log.write(
          `${Timer.display(start) + registerRoute} ${JSON.stringify(
            errors.array(),
            null,
            4
          )}`
        );
        res.status(422).json({ errors: errors.array() });
      }
      log.write(
        `${registerRoute} ` +
          `Creating User: ${JSON.stringify(req.body, null, 4)}`
      );
      User.create(req.body.username, req.body.password)
        .then(function(value) {
          res.status(201).json(value);
          log.write(
            `${Timer.display(start) + registerRoute} ${value.username} Created`
          );
        })
        .catch(function(CreateErr) {
          console.log(CreateErr.message);
          res.status(400).json({ err: CreateErr.message });
          log.write(
            `${Timer.display(start) + registerRoute} ${req.body.username} ${
              CreateErr.message
            }`
          );
          log.write(
            `${Timer.display(start) + registerRoute} ${
              req.body.username
            } not Created`
          );
        });
    }
  );
  /**
   * @swagger
   * /user/login:
   *   post:
   *     tags:
   *       - user
   *     description: Login as user
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: user
   *         description: user to login
   *         in: body
   *         required: true
   *         schema:
   *           $ref: '#/definitions/user'
   *     responses:
   *       201:
   *         description: User login
   *         schema:
   *           $ref: '#/definitions/userLogin'
   *       422:
   *         description: Error Code
   *         schema:
   *           $ref: '#/definitions/error'
   */
  app.post(
    loginRoute,
    [
      check("username").exists(),
      check("password").exists(),
      check("username", "USERNAME_NOT_EXISTING").custom(username => {
        return User.isUsernameExist(username).then(value => {
          return value;
        });
      })
    ],
    cors(),
    function(req, res) {
      start = Date.now();
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        log.write(
          `${Timer.display(start) + registerRoute} ${JSON.stringify(
            errors.array(),
            null,
            4
          )}`
        );
        res.status(422).json({ errors: errors.array() });
      }
      log.write(
        `${loginRoute} Login User: ${JSON.stringify(req.body, null, 4)}`
      );
      User.login(req.body.username, req.body.password)
        .then(function(value) {
          res.status(200).json(value);
          log.write(`${Timer.display(start) + loginRoute} ${value}`);
        })
        .catch(function(err) {
          console.log(err);
          res.status(400).json({ err });
        });
    }
  );

  /**
   * @swagger
   * /user/get:
   *   get:
   *     tags:
   *       - user
   *     description: user
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: User
   *       422:
   *         description: Error Code
   *         type: string
   *
   */
  app.get(
    getUserRoute,
    [
      check("token").exists(),
      check("token", "TOKEN_ERROR").custom(token => {
        return User.verifyToken(token).then(value => {
          return value;
        });
      })
    ],
    cors(),
    function(req, res) {
      start = Date.now();
      User.getUserfromToken(req.headers.token).then(function(user) {
        User.getUserFromID(user.id).then(function(value) {
          res.json(value);
          log.write(
            `${Timer.display(start) + getUserRoute} ${
              req.headers.token
            } ${value}`
          );
        });
      });
    }
  );

  /**
   * @swagger
   * /user/edit:
   *   put:
   *     tags:
   *       - user
   *     description: user
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: User
   *       422:
   *         description: Error Code
   *         type: string
   *
   */
  app.put(
    editUserRoute,
    [
      check("token").exists(),
      check("token", "TOKEN_ERROR").custom(token => {
        return User.verifyToken(token).then(value => {
          return value;
        });
      })
    ],
    cors(),
    function(req, res) {
      start = Date.now();
      User.getUserfromToken(req.headers.token).then(function(user) {
        User.edit(user.id, req.body.username, req.body.password).then(function(
          value
        ) {
          res.json({
            msg: value
          });

          log.write(
            `${Timer.display(start) + editUserRoute} ${
              user.username
            } ${JSON.stringify(value, null, 4)}`
          );
        });
      });
    }
  );

  /**
   * @swagger
   * /user/delete:
   *   delete:
   *     tags:
   *       - user
   *     description: Delete user
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: User deleted
   *       422:
   *         description: Error Code
   *         type: string
   *
   */
  app.delete(
    destroyRoute,
    [
      check("token").exists(),
      check("token", "TOKEN_ERROR").custom(token => {
        return User.verifyToken(token).then(value => {
          return value;
        });
      })
    ],
    cors(),
    function(req, res) {
      start = Date.now();
      User.delete(req.body.username).then(function(value) {
        res.json({
          msg: value
        });
        log.write(
          `${Timer.display(start) + destroyRoute} ${req.body.username} ${value}`
        );
      });
    }
  );
};
