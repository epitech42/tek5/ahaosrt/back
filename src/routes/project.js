// Import Node Modules
const { check, validationResult } = require("express-validator/check");

// Import Actions
const Project = require("../actions/project.js");
const User = require("../actions/user.js");

// Import Misc Actions
const Timer = require("../actions/timer.js");
const log = require("../actions/notifications.js");

let start = new Date();

/* ROUTES */
const registerRoute = "/project/create";
const loginRoute = "/project/login";
const destroyRoute = "/project/delete";
const projectListRoute = "/project/list";
const addUsersRoute = "/project/users/add";
const editProjectRoute = "/project/edit";

module.exports = function(app, cors) {
  /**
   * @swagger
   * /project/create:
   *   post:
   *     tags:
   *       - project
   *     description: Creates a new project
   *     produces:
   *       - application/json
   *     security:
   *       - tokenAuth: []
   *     parameters:
   *       - name: project
   *         description: project to create
   *         in: body
   *         required: true
   *         schema:
   *           $ref: '#/definitions/projectCreate'
   *     responses:
   *       201:
   *         description: Project Created
   *         schema:
   *           $ref: '#/definitions/projectCreated'
   *       422:
   *         description: Error
   *         schema:
   *           $ref: '#/definitions/error'
   */
  app.post(
    registerRoute,
    [
      check("title").exists(),
      check("token").custom(token => {
        return User.verifyToken(token).then(value => {
          return value;
        });
      })
    ],
    cors(),
    function(req, res) {
      start = Date.now();
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        log.write(
          `${Timer.display(start) + registerRoute} ${JSON.stringify(
            errors.array(),
            null,
            4
          )}`
        );
        res.status(422).json({ errors: errors.array() });
      }
      log.write(
        `${registerRoute} ` +
          `Creating Project: ${JSON.stringify(req.body, null, 4)}`
      );
      User.getUserfromToken(req.headers.token).then(function(user) {
        Project.create(
          req.body.title,
          req.body.url,
          user.id,
          req.body.description
        )
          .then(function(value) {
            res.status(201).json(value);
            log.write(
              `${Timer.display(start) + registerRoute} Project ${
                value.projectID
              } Created`
            );
          })
          .catch(function(CreateErr) {
            console.log(CreateErr.message);
            res.status(400).json({ err: CreateErr.message });
            log.write(
              `${Timer.display(start) + registerRoute} ${req.body.username} ${
                CreateErr.message
              }`
            );
            log.write(
              `${Timer.display(start) + registerRoute} ${
                req.body.username
              } not Created`
            );
          });
      });
    }
  );

  /**
   * @swagger
   * /project/login:
   *   post:
   *     tags:
   *       - project
   *     description: Login to a new project
   *     produces:
   *       - application/json
   *     security:
   *       - tokenAuth: []
   *     parameters:
   *       - name: project
   *         description: project to login
   *         in: body
   *         required: true
   *         schema:
   *           $ref: '#/definitions/projectLogin'
   *     responses:
   *       201:
   *         description: Project Token to send on Socket
   *         schema:
   *           $ref: '#/definitions/projectLoginSuccess'
   *       422:
   *         description: Error
   *         schema:
   *           $ref: '#/definitions/error'
   */

  app.post(
    loginRoute,
    [
      check("projectID").exists(),
      check("token").exists(),
      check("token", "TOKEN_ERROR").custom(token => {
        return User.verifyToken(token).then(value => {
          return value;
        });
      }),
      check("projectID", "PROJECT_NOT_FOUND").custom(projectID => {
        return Project.isProjectExist(projectID).then(function(value) {
          return value;
        });
      }),
      check("projectID", "USER_NOT_ON_PROJECT").custom((projectID, token) => {
        return Project.isUserOnProject(projectID, token).then(function(value) {
          return value;
        });
      })
    ],
    cors(),
    function(req, res) {
      start = Date.now();
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        log.write(
          `${Timer.display(start) + registerRoute} ${JSON.stringify(
            errors.array(),
            null,
            4
          )}`
        );
        res.status(422).json({ errors: errors.array() });
      }
      log.write(
        `${registerRoute} ` +
          `Creating Project: ${JSON.stringify(req.body, null, 4)}`
      );
      User.getUserfromToken(req.headers.token).then(function(user) {
        Project.generateToken(user, req.body.projectID).then(function(value) {
          res.json({ token: value });
          log.write(
            `${Timer.display(start) + loginRoute} ${req.body.projectID} ${
              user.username
            } Login`
          );
        });
      });
    }
  );

  /**
   * @swagger
   * /project/users/add:
   *   post:
   *     tags:
   *       - project
   *     description: Login to a new project
   *     produces:
   *       - application/json
   *     security:
   *       - tokenAuth: []
   *     parameters:
   *       - name: project
   *         description: project to login
   *         in: body
   *         required: true
   *         schema:
   *           $ref: '#/definitions/projectAddUsers'
   *     responses:
   *       201:
   *         description: Project
   *         schema:
   *           $ref: '#/definitions/projectAddUsersSuccess'
   *       422:
   *         description: Error
   *         schema:
   *           $ref: '#/definitions/error'
   */

  app.post(
    addUsersRoute,
    [
      check("projectID").exists(),
      check("projectID", "PROJECT_NOT_FOUND").custom(projectID => {
        return Project.isProjectExist(projectID).then(function(value) {
          return value;
        });
      }),
      check("token").exists(),
      check("users", "USERS_NOT_FOUND").custom(users => {
        for (let i = 0; i < users.length; i += 1) {
          console.log(users[i]);
        }
        return true;
      }),
      check("token").custom(token => {
        return User.verifyToken(token).then(value => {
          return value;
        });
      })
    ],
    cors(),
    function(req, res) {
      start = Date.now();
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        log.write(
          `${Timer.display(start) + addUsersRoute} ${JSON.stringify(
            errors.array(),
            null,
            4
          )}`
        );
        res.status(422).json({ errors: errors.array() });
      }
      log.write(
        `${registerRoute} ` +
          `Creating Project: ${JSON.stringify(req.body, null, 4)}`
      );
      Project.addUsers(req.body.projectID, req.body.users).then(function(
        value
      ) {
        res.status(200).json(value);
        log.write(
          `${Timer.display(start) + addUsersRoute} ${req.body.projectID} ${
            req.body.users
          }`
        );
      });
    }
  );

  /**
   * @swagger
   * /project/list:
   *   get:
   *     tags:
   *       - project
   *     description: List Users Project
   *     produces:
   *       - application/json
   *     security:
   *       - tokenAuth: []
   *     responses:
   *       201:
   *         description: List Project
   *         schema:
   *           $ref: '#/definitions/ListProject'
   *       422:
   *         description: Error
   *         schema:
   *           $ref: '#/definitions/error'
   */

  app.get(
    projectListRoute,
    [
      check("token").custom(token => {
        return User.verifyToken(token).then(value => {
          return value;
        });
      })
    ],
    cors(),
    function(req, res) {
      start = Date.now();
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        log.write(
          `${Timer.display(start) + registerRoute} ${JSON.stringify(
            errors.array(),
            null,
            4
          )}`
        );
        res.status(422).json({ errors: errors.array() });
      }
      User.getUserfromToken(req.headers.token).then(function(user) {
        Project.getProjectsFromUserID(user.id)
          .then(function(projects) {
            res.status(200).json(projects);
            log.write(
              `${Timer.display(start) + projectListRoute} ${user.username}`
            );
          })
          .catch(function(CreateErr) {
            console.log(CreateErr.message);
            res.status(400).json({ err: CreateErr.message });
            log.write(
              `${Timer.display(start) + projectListRoute} ${user.username} ${
                CreateErr.message
              }`
            );
            log.write(
              `${Timer.display(start) + projectListRoute} ${
                user.username
              } not Created`
            );
          });
      });
    }
  );

  /**
   * @swagger
   * /project/edit:
   *   put:
   *     tags:
   *       - project
   *     description: project edit
   *     produces:
   *       - application/json
   *     responses:
   *       200:
   *         description: User
   *       422:
   *         description: Error Code
   *         type: string
   *
   */
  app.put(
    editProjectRoute,
    [
      check("token").exists(),
      check("token", "TOKEN_ERROR").custom(token => {
        return User.verifyToken(token).then(value => {
          return value;
        });
      })
    ],
    cors(),
    function(req, res) {
      start = Date.now();
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        log.write(
          `${Timer.display(start) + destroyRoute} ${JSON.stringify(
            errors.array(),
            null,
            4
          )}`
        );
        res.status(422).json({ errors: errors.array() });
      }
      start = Date.now();
      User.getUserfromToken(req.headers.token).then(function(user) {
        Project.isUserOnProject(req.body.projectID, user.id).then(function(
          authorized
        ) {
          if (authorized === true) {
            Project.edit(
              req.body.projectID,
              req.body.title,
              req.body.url,
              req.body.description
            ).then(function(value) {
              res.json({
                msg: value
              });
              log.write(
                `${Timer.display(start) + editProjectRoute} ${
                  user.username
                }}${JSON.stringify(value, null, 4)}`
              );
            });
          } else {
            res.status(422).json({ error: "USER_NOT_ON_PROJECT" });
            log.write(
              `${Timer.display(start) + editProjectRoute} ${
                user.username
              }} ${JSON.stringify(authorized, null, 4)}`
            );
          }
        });
      });
    }
  );

  /**
   * @swagger
   * /project/delete:
   *   delete:
   *     tags:
   *       - project
   *     description: Delete Project
   *     security:
   *       - tokenAuth: []
   *     produces:
   *       - application/json
   *     parameters:
   *       - name: user
   *         description: user to delete
   *         in: body
   *         required: true
   *         schema:
   *           $ref: '#/definitions/userDestroy'
   *     responses:
   *       200:
   *         description: User deleted
   *       422:
   *         description: Username not found
   *
   */
  app.delete(
    destroyRoute,
    [
      check("projectID").exists(),
      check("projectID", "PROJECT_NOT_FOUND").custom(projectID => {
        return Project.isProjectExist(projectID).then(function(value) {
          return value;
        });
      }),
      check("token").exists(),
      check("token").custom(token => {
        return User.verifyToken(token).then(value => {
          return value;
        });
      })
    ],
    cors(),
    function(req, res) {
      start = Date.now();
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        log.write(
          `${Timer.display(start) + destroyRoute} ${JSON.stringify(
            errors.array(),
            null,
            4
          )}`
        );
        res.status(422).json({ errors: errors.array() });
      }
      User.getUserfromToken(req.headers.token).then(function(user) {
        Project.delete(req.body.projectID).then(function(value) {
          res.json({
            msg: value
          });
          log.write(
            `${Timer.display(start) + destroyRoute} ${user.username} ${value}`
          );
        });
      });
    }
  );
};
