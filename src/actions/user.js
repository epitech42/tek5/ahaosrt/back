// Import Node Modules
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

// Import User Model
const UserModel = require("../models/User.js");

const cache = require("../db/cache.js").client;

module.exports = {
  async create(username, password) {
    return new Promise(function(resolve, reject) {
      UserModel.find({ username }, function(error, comments) {
        if (comments[0] !== undefined) {
          reject(new Error("USER_FOUND"));
        } else {
          // username = username + '#' + Random.num(5);
          bcrypt.hash(password, 10, function(err, hash) {
            const user = new UserModel({ username, password: hash });
            cache.set(username, user);
            resolve({
              message: "USER_CREATED",
              username
            });
            user.save().then(() => {
              console.log("user save in DB");
              cache.set(user._id, user.username);
            });
          });
        }
      });
    }).catch(function(err) {
      throw new Error(err);
    });
  },
  getUsernameFromID(id) {
    return new Promise(function(resolve, reject) {
      cache.get(id).then(function(err, value) {
        if (value !== undefined && value !== null) {
          resolve(value);
        } else {
          UserModel.find({ _id: id }, function(error, comments) {
            if (comments[0] === undefined) reject(new Error("USER_NOT_FOUND"));
            else {
              resolve(comments[0].username);
            }
          });
        }
      });
    }).catch(function(err) {
      throw new Error(err);
    });
  },
  delete(username) {
    return new Promise(function(resolve, reject) {
      UserModel.find({ username }, function(error, comments) {
        if (comments[0] === undefined) reject(new Error("USER_NOT_FOUND"));
        else {
          UserModel.deleteOne({ username }, function(error, comments) {
            if (comments !== undefined) resolve("USER_DELETED");
            else reject(new Error(error));
          });
          // UserModel.updateMany({"username": username}, {$set: {serverEncryptionKey: 'NULL', active: false}}, function(error, comments){
          // resolve("Delete");
          // });
        }
      });
    }).catch(function(err) {
      throw new Error(err);
    });
  },
  isUsernameExist(username) {
    return new Promise(function(resolve, reject) {
      // CHECKING CACHE
      cache.get(username, (err, result) => {
        if (result !== undefined && result !== null) {
          resolve(true);
        } else {
          // CACHING
          UserModel.find({ username }, function(error, comments) {
            if (comments !== undefined) {
              cache.set(username, JSON.stringify(comments[0]));
              resolve(true);
            } else {
              resolve(false);
            }
          });
        }
      });
    });
  },
  login(username, password) {
    return new Promise(function(resolve, reject) {
      UserModel.find({ username }, function(error, comments) {
        if (comments[0] !== undefined) {
          bcrypt
            .compare(password, comments[0].password)
            .then(result => {
              if (result === true) {
                jwt.sign(
                  { id: comments[0]._id, username: comments[0].username },
                  process.env.SECRET,
                  { expiresIn: "1h" },
                  function(err, token) {
                    console.log(token);
                    resolve({ username, token });
                    // UserModel.updateMany({_id: comments[0]._id}, {$set: {token: 'NULL', active: false}}, function(error, comments){

                    // });
                  }
                );
              } else reject(new Error("WRONG_PASSWORD"));
              /* jwt.sign(comments[0], comments[0].password, { expiresIn: '1h'},function(err, token) {
			  console.log(token);
			  resolve(token);
			  }); */
            })
            .catch(function() {
              reject(new Error("WRONG_PASSWORD"));
            });
        } else reject(new Error("USER_NOT_FOUND"));
      });
    }).catch(function(err) {
      throw new Error(err);
    });
  },
  verifyToken(token) {
    return new Promise(function(resolve, reject) {
      // UserModel.find({_id: id}, function(err, comments){
      jwt.verify(token, process.env.SECRET, function(err, decoded) {
        if (decoded !== undefined) resolve(true);
        else reject(false);
      });
      // });
    });
  },
  getUserfromToken(token) {
    return new Promise(function(resolve, reject) {
      jwt.verify(token, process.env.SECRET, function(err, decoded) {
        if (decoded !== undefined) resolve(decoded);
        else resolve(false);
      });
    });
  },
  isSocketIDConnected(clientID) {
    return new Promise(function(resolve, reject) {
      UserModel.find({ WebSocketID: clientID }, function(error, comments) {
        if (comments[0] !== undefined && comments[0] !== null) resolve(true);
        else resolve(false);
      });
    });
  },
  edit(id, username, password) {
    return new Promise(function(resolve, reject) {
      UserModel.find({ _id: id }, function(error, comments) {
        username |= comments[0].username;
        password |= comments[0].password;
        UserModel.updateMany(
          { _id: id },
          { $set: { username, password } },
          function(error, comments) {
            if (comments !== undefined) resolve(comments);
            else reject("ERROR");
          }
        );
      });
    });
  },
  search(string) {
    return new Promise(function(resolve, reject) {
      UserModel.find(
        { username: { $regex: string } },
        { _id: 1, username: 1 },
        function(error, users) {
          if (users !== undefined) resolve(users);
          else reject("NO_USER");
        }
      );
    });
  },
  getUserFromClientID(WebSocketID) {
    return new Promise(function(resolve, reject) {
      UserModel.find({ WebSocketID }, function(error, comments) {
        if (comments[0] !== null) resolve(comments[0]);
        else reject(new Error(false));
      });
    });
  },
  connect(userID, clientID) {
    return new Promise(function(resolve, reject) {
      UserModel.updateMany(
        { _id: userID },
        { $set: { connected: true, WebSocketID: clientID } },
        function(error, comments) {
          resolve("CONNECTED");
        }
      );
    });
  },
  disconnect(clientID) {
    return new Promise(function(resolve, reject) {
      UserModel.updateMany(
        { WebSocketID: clientID },
        { $set: { connected: false, WebSocketID: null } },
        function(error, comments) {
          resolve("DISCONNECTED");
        }
      );
    });
  },
  getUserFromID(id) {
      return new Promise(function(resolve, reject) {
	  UserModel.find({ _id: id }, {"password": 0, "__v": 0}, function(error, comments) {
	      if (comments[0] !== undefined && comments[0] !== null)
		  resolve(comments[0]);
	      else
		  reject("USER_NOT_FOUND");
	  });
    });
  }
};
