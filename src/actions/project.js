// Import Node Modules
const jwt = require("jsonwebtoken");

const User = require("./user.js");

// Import User Model
const UserModel = require("../models/User.js");
const ProjectModel = require("../models/Project.js");

const cache = require("../db/cache.js").client;

const { promisify } = require("util");

const getAsync = promisify(cache.get).bind(cache);

const { ObjectID } = require("mongodb");

const Project = {
  async create(title, url, userID, description) {
    return new Promise(function(resolve, reject) {
      const project = new ProjectModel({
        miniature: "https://wallpaperplay.com/walls/full/a/9/9/3080.jpg",
        title,
        url,
        description,
        owner: userID,
        createdBy: userID,
        updatedBy: userID
      });
      project
        .save()
        .then(() => {
          cache.set(project._id.toString(), project.toString());
          resolve({
            message: "PROJECT_CREATED",
            projectID: project._id
          });
        })
        .catch(function(err) {
          reject(new Error(err));
        });
    });
  },
  getProjectFromID(id) {
    return new Promise(function(resolve, reject) {
      getAsync(id).then(function(err, value) {
        if (value !== undefined && value !== null) {
          console.log("project in redis");
          resolve(value);
        } else {
          ProjectModel.find({ _id: id }, function(error, comments) {
            if (comments[0] !== undefined || comments[0] !== null) {
              console.log("project in mongo");
              resolve(comments[0]);
            } else reject(new Error("PROJECT_NOT_FOUND"));
          });
        }
      });
    }).catch(function(err) {
      throw new Error(err);
    });
  },
  delete(projectID) {
    return new Promise(function(resolve, reject) {
      ProjectModel.find({ _id: projectID }, function(error, comments) {
        if (comments[0] === null || comments[0] === undefined)
          reject(new Error("PROJECT_NOT_FOUND"));
        else {
          console.log(comments[0]);
          UserModel.deleteOne({ _id: projectID }, function(error, comments) {
            console.log(`${JSON.stringify(comments, null, 4)}`);
            resolve("PROJECT_DELETED");
          });
        }
      });
    }).catch(function(err) {
      throw new Error(err);
    });
  },
  isProjectExist(id) {
    return new Promise(function(resolve, reject) {
      cache.get(id, (err, result) => {
        if (result !== undefined && result !== null) {
          resolve(true);
        } else {
          ProjectModel.find({ _id: id }, function(error, comments) {
            if (comments !== undefined) {
              // cache.set(id, comments[0].toString());
              resolve(true);
            } else {
              resolve(false);
            }
          });
        }
      });
    });
  },
  getProjectsFromUserID(id) {
    return new Promise(function(resolve, reject) {
      ProjectModel.find(
        { owner: id },
        {
          title: 1,
          _id: 1,
          users: 1,
          owner: 1,
          miniature: 1,
          created: 1,
          updated: 1,
          description: 1
        },
        function(error, projects) {
          ProjectModel.find(
            { users: id },
            {
              title: 1,
              _id: 1,
              users: 1,
              owner: 1,
              miniature: 1,
              created: 1,
              updated: 1,
              description: 1
            },
            function(error, comments) {
              resolve(projects.concat(comments));
            }
          );
        }
      );
    });
  },
  AddSubtitle(projectID, beginTime, endTime, content, createdBy) {
    return new Promise(function(resolve, reject) {
      ProjectModel.find({ _id: projectID }, function(error, projects) {
        if (projects !== undefined) {
          const _id = new ObjectID();
          projects[0].subtitles.push({
            _id,
            beginTime,
            endTime,
            content,
            createdBy,
            updatedBy: createdBy
          });
          projects[0].save().then(function(value) {
            console.log(value);
            resolve(value.subtitles.find(subtitle => subtitle._id === _id));
          });
        } else {
          reject(new Error("PROJECT_NOT_FOUND"));
        }
      });
    });
  },
  EditSubtitle(projectID, subtitleID, beginTime, endTime, content, updatedBy) {
    return new Promise(function(resolve, reject) {
      ProjectModel.findOneAndUpdate(
        { _id: projectID, "subtitles._id": subtitleID },
        {
          $set: { beginTime, endTime, content, updatedBy, updated: new Date() }
        },
        function(error, value) {
          resolve(
            value.subtitles.find(subtitle => subtitle._id === subtitleID)
          );
        }
      );
    });
  },
  DeleteSubtitle(projectID, subtitleID) {
    return new Promise(function(resolve, reject) {
      ProjectModel.find({ _id: projectID }, function(error, projects) {
        if (projects !== undefined) {
          Page.update(
            {
              _id: projectID,
              "subtitles._id": subtitleID
            },
            {
              $pull: { subtitles: { _id: subtitleID } }
            },
            function(error, result) {
              console.log(result);
              resolve(result);
            }
          );
        } else {
          reject(new Error("PROJECT_NOT_FOUND"));
        }
      });
    });
  },
  addUsers(projectID, users) {
    return new Promise(function(resolve, reject) {
      ProjectModel.find({ _id: projectID }, function(error, comments) {
        if (comments[0] !== undefined) {
          for (i = 0; i < comments[0].users.length; i++) {
            for (n = 0; n < users; n++) {
              if (users[n] === comments[0].users[i])
                reject("USER_ALREADY_IN_PROJECT");
            }
          }
          comments[0].users = comments[0].users.concat(users);
          comments[0].save().then(function(error, value) {
            resolve(value);
          });
        }
      });
    });
  },
  generateToken(user, projectID) {
    return new Promise(function(resolve, reject) {
      jwt.sign(
        { projectID, username: user.username, id: user.id },
        process.env.SECRET,
        { expiresIn: "1h" },
        function(err, token) {
          if (token !== null && token !== undefined) {
            console.log(token);
            resolve(token);
          } else {
            reject(new Error("TOKEN_FAILED"));
          }
        }
      );
    });
  },
  isUserOnProject(projectID, token) {
    return new Promise(function(resolve, reject) {
      User.getUserfromToken(token).then(function(user) {
        ProjectModel.find({ _id: projectID }, function(error, comments) {
          console.log(JSON.stringify(comments));
          resolve(true);
        });
      });
    });
  },
  edit(id, title, url, description) {
    return new Promise(function(resolve, reject) {
      ProjectModel.find({ _id: id }, function(error, comments) {
        if (comments[0] !== undefined) {
          title = title || comments[0].title;
          url = url || comments[0].url;
          description = description || comments[0].description;
          ProjectModel.updateMany(
            { _id: id },
            { $set: { title, url, description } },
            function(error, comments) {
              if (comments !== undefined) resolve(comments);
              else reject("ERROR");
            }
          );
        }
      });
    });
  }
};

module.exports = Project;
