// SOCKET IO
const io = require("socket.io")();
const redisAdapter = require("socket.io-redis");

io.adapter(redisAdapter({ host: "redis", port: 6379 }));

// Import Actions
const User = require("../../actions/user.js");
const Project = require("../../actions/project.js");

// Import Misc Actions
const Timer = require("../../actions/timer.js");
const log = require("../../actions/notifications.js");

const cache = require("../../db/cache.js").client;

const userLoginEvent = "user:login";
const userDisconnectEvent = "disconnect";

const subtitleAddEvent = "subtitle:add";
const subtitleEditEvent = "subtitle:edit";
const subtitleDeleteEvent = "subtitle:delete";

const userGetInfosEvent = "user:getinfos";

const projectLoadEvent = "project:load";

const success = ":success";
const err = ":err";

let start = new Date();

module.exports = function(client) {
  client.on(userLoginEvent, token => {
    start = Date.now();
    User.verifyToken(token).then(value => {
      if (value === true) {
        User.getUserfromToken(token).then(function(user) {
          log.write(`username: ${user.username}`);
          User.connect(user.id, client.id).then(function(value) {
            client.emit(userLoginEvent + success, value);
            client.join(user.projectID);
            log.write(
              `${Timer.display(start)}[${client.id}]` +
                ` ${user.username} ${userLoginEvent} ${value}`
            );
          });
        });
      } else {
        client.emit(userLoginEvent + err, "NOT_CONNECTED");
        log.write(
          `${Timer.display(start)}[${client.id}]` +
            ` ${userLoginEvent} ` +
            `NOT_CONNECTED`
        );
      }
    });
  });

  client.on(subtitleAddEvent, function(beginTime, endTime, content) {
    console.log(`Receive on ${client.id} new subtitle`);
    User.isSocketIDConnected(client.id).then(value => {
      console.log(value);
      if (value === true) {
        const projectID = client.id.substring(9, client.id.search("#"));
        User.getUserFromClientID(client.id).then(function(user) {
          console.log(client.id);
          console.log(JSON.stringify(user));
          Project.AddSubtitle(projectID, beginTime, endTime, content, user._id)
            .then(function(value) {
              client.emit(subtitleAddEvent + success, value);
              client.broadcast.emit(subtitleAddEvent, value);
            })
            .catch(function(err) {
              console.log(err);
              client.emit(subtitleAddEvent + err, err);
            });
        });
      } else {
        client.emit(subtitleAddEvent + err, "USER_NOT_CONNECTED");
      }
    });
  });

  client.on(subtitleEditEvent, function(
    subtitleID,
    beginTime,
    endTime,
    content
  ) {
    console.log(`Receive on ${client.id} new subtitle`);
    User.isSocketIDConnected(client.id).then(value => {
      console.log(value);
      if (value === true) {
        const projectID = client.id.substring(9, client.id.search("#"));
        console.log(projectID);
        User.getUserFromClientID(client.id).then(function(user) {
          Project.EditSubtitle(
            projectID,
            subtitleID,
            beginTime,
            endTime,
            content,
            user._id
          )
            .then(function(value) {
              client.emit(subtitleEditEvent + success, value);
              client.broadcast.emit(subtitleEditEvent, value);
            })
            .catch(function(err) {
              console.log(err);
              client.emit(subtitleEditEvent + err, err);
            });
        });
      } else {
        client.emit(subtitleEditEvent + err, "USER_NOT_CONNECTED");
      }
    });
  });

  client.on(projectLoadEvent, function() {
    console.log(`Receive on ${client.id} load projectx`);
    User.isSocketIDConnected(client.id).then(value => {
      console.log(value);
      if (value === true) {
        const projectID = client.id.substring(9, client.id.search("#"));
        User.getUserFromClientID(client.id).then(function(user) {
          console.log(projectID);
          Project.getProjectFromID(projectID)
            .then(function(value) {
              client.emit(projectLoadEvent + success, value);
              // client.broadcast.emit(projectLoadEvent, value);
            })
            .catch(function(err) {
              console.log(err);
              client.emit(projectLoadEvent + err, err);
            });
        });
      } else {
        client.emit(projectLoadEvent + err, "USER_NOT_CONNECTED");
      }
    });
  });

  client.on(subtitleDeleteEvent, function(subtitleID) {
    User.isSocketIDConnected(client.id).then(value => {
      console.log(value);
      if (value === true) {
        const projectID = client.id.substring(9, client.id.search("#"));
        console.log(projectID);
        User.getUserFromClientID(client.id).then(function(user) {
          Project.DeleteSubtitle(projectID, subtitleID, user._id)
            .then(function(value) {
              client.emit(subtitleDeleteEvent + success, { id: subtitleID });
              client.broadcast.emit(subtitleDeleteEvent, { id: subtitleID });
            })
            .catch(function(err) {
              console.log(err);
              client.emit(subtitleDeleteEvent + err, err);
            });
        });
      } else {
        client.emit(subtitleDeleteEvent + err, "USER_NOT_CONNECTED");
      }
    });
  });

  client.on(userGetInfosEvent, function(userID) {
      start = Date.now();
      //const users = [];
      User.isSocketIDConnected(client.id).then(value => {
	  var users = [];
	  console.log(value);
	  if (value === true) {
              const projectID = client.id.substring(9, client.id.search("#"));
              console.log(projectID);
	      console.log(JSON.stringify(userID));
	      for (i = 0; i < userID.length; i++){
		  User.getUserFromID(userID[i]).then(function(user) {
		      users.push(user);
		      console.log(user);
		  }).catch(function(err){
		      console.log(userID[i] + " " + err);
		  });
	      }
	      setTimeout(function(){
		  client.emit(userGetInfosEvent + success, users);
		  log.write(
		      `${Timer.display(start)}[${client.id}]` +
			  ` ${userGetInfosEvent} ${JSON.stringify(users)}`
		  );
	      }, 20);
	  }
      });
  });
					      

  client.on(userDisconnectEvent, function() {
    start = Date.now();
    log.write(client.id);
    cache.del(client.id);
    User.isSocketIDConnected(client.id).then(function(value) {
      if (value === true) {
        User.disconnect(client.id).then(function(value) {
          log.write(
            `${Timer.display(start)}[${client.id}]` +
              ` ${userDisconnectEvent} ${value}`
          );
        });
      } else {
        log.write(
          `${Timer.display(start)}[${client.id}] ${userDisconnectEvent}`
        );
      }
    });
  });
};
