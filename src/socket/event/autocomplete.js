// SOCKET IO
const io = require("socket.io")();
const redisAdapter = require("socket.io-redis");

io.adapter(redisAdapter({ host: "redis", port: 6379 }));

// Import Actions
const User = require("../../actions/user.js");

// Import Misc Actions
const Timer = require("../../actions/timer.js");
const log = require("../../actions/notification.js");

const userSearchEvent = "user:search";

const success = ":success";
const err = ":err";

let start = new Date();

module.exports = function(client) {
  client.on(userSearchEvent, string => {
    start = Date.now();
    User.search(string)
      .then(function(users) {
        client.emit(userSearchEvent + success, users);
      })
      .catch(function(error) {
        client.emit(userSearchEvent + err, error);
      });
  });
};
