/* eslint global-require: "warn" */

// Import Misc Actions

// eslint-disable-line global-require
const io = require("socket.io")();
const redisAdapter = require("socket.io-redis");

io.adapter(redisAdapter({ host: "redis", port: 6379 }));

// const start = Date.now();

io.of("/").on("connect", socket => {
  console.log(`New Client ${socket.id} on AutoComplete connected on WebSocket`);
  require("./socket/event/autocomplete.js")(socket);
});

io.of(/^\/project-(.)+$/).on("connect", socket => {
  console.log(`New Client ${socket.id} connected on WebSocket`);
  require("./socket/event/project.js")(socket);
});

io.listen(1337);

// EXPRESS API REST
const express = require("express");

const cors = require("cors");

// Import Swagger
const swaggerUi = require("swagger-ui-express");
const swaggerJSDoc = require("swagger-jsdoc");

const port = process.env.PORT || 3000;
const app = express();

// Decode body request with json
app.use(express.json());

app.options("*", cors());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "X-Requested-With",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

require("./routes/user.js")(app, cors);
require("./routes/project.js")(app, cors);

const swaggerDefinition = {
  info: {
    title: "AhaoSRT API",
    version: "1.0.0",
    description: "Docs to use AhaoSRT API"
  },
  // host: 'localhost:3000',
  basePath: "/"
};

// options for the swagger docs
const options = {
  // import swaggerDefinitions
  swaggerDefinition,
  // path to the API docs
  apis: ["index.js", "routes/*.js"] // pass all in array
};

// initialize swagger-jsdoc
const swaggerSpec = swaggerJSDoc(options);

app.get("/", (req, res) => res.send("AhaoSRT API"));
app.get("/swagger.json", function(req, res) {
  res.setHeader("Content-Type", "application/json");
  res.send(swaggerSpec);
});
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerSpec));

app.get("/health", function(req, res) {
  res.setHeader("Content-Type", "application/json");
  res.send({ msg: "I'm alive" });
});

app.listen(port, () =>
  console.log(`AhaoSRT API started\nListening on port ${port} !`)
);

/**
 * @swagger
 * definitions:
 *   users:
 *     properties:
 *         type: array
 *         items:
 *           type: string
 *   user:
 *     properties:
 *       username:
 *         type: string
 *       password:
 *         type: string
 *   userLogin:
 *     properties:
 *       token:
 *         type: string
 *   userDestroy:
 *     properties:
 *       username:
 *         type: string
 *       destroyPassphrase:
 *         type: string
 *   userCreated:
 *    properties:
 *      msg:
 *        type: string
 *      username:
 *        type: string
 *   projectCreate:
 *    properties:
 *     url:
 *        type: string
 *     title:
 *        type: string
 *   projectCreated:
 *    properties:
 *      msg:
 *        type: string
 *      projectID:
 *        type: string
 *   error:
 *    properties:
 *      err:
 *        type: string
 *   projectLogin:
 *    properties:
 *      projectID:
 *        type: string
 *   projectLoginSuccess:
 *    properties:
 *      token:
 *        type: string
 *   projectAddUsers:
 *     properties:
 *       projectID:
 *         type: string
 *       users:
 *         type: array
 *         items:
 *           type: string
 */
