const db = require("../db/db.js");

const Subtitles = new db.Schema({
  beginTime: { type: String, required: true },
  endTime: { type: String, required: true },
  content: { type: String, required: true },
  created: { type: Date, required: true, default: Date.now },
  updated: { type: Date, required: true, default: Date.now },
  createdBy: { type: String, required: true },
  updatedBy: { type: String, required: true }
});

const Project = db.model("Project", {
  title: { type: String, required: true },
  description: { type: String, required: false },
  users: { type: [String], required: true },
  owner: { type: String, required: true },
  subtitles: { type: [Subtitles], required: false },
  url: { type: String, required: true },
  miniature: { type: String, require: true },
  created: { type: Date, required: true, default: Date.now },
  updated: { type: Date, required: true, default: Date.now },
  createdBy: { type: String, required: true },
  updatedBy: { type: String, required: true }
});
module.exports = Project;
