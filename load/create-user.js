var crypto = require('crypto')
var openpgp = require('openpgp'); // use as CommonJS, AMD, ES6 module or via window.openpgp

module.exports = {
    generateUser: generateUser,
    setUsername: setUsername,
    setPublicKey: setPublicKey
}

function setUsername(requestParams, context, ee, next) {
    context.vars['username'] =  crypto
        .randomBytes(Math.ceil(20 / 2))
        .toString('hex') // convert to hexadecimal format
        .slice(0, 20);    
    return next(); // MUST be called for the scenario to continue
}

function setPublicKey(requestParams, response, context, ee, next) {
    var options = {
	userIds: [{ name:'Jon Smith', email:'jon@example.com' }], // multiple user IDs
	curve: "ed25519",                                         // ECC curve name
	passphrase: 'super long and hard to guess secret'         // protects the private key
    };
    openpgp.generateKey(options).then(function(key) {
	var privkey = key.privateKeyArmored; // '-----BEGIN PGP PRIVATE KEY BLOCK ... '
	var pubkey = key.publicKeyArmored;   // '-----BEGIN PGP PUBLIC KEY BLOCK ... '
	var revocationCertificate = key.revocationCertificate; // '-----BEGIN PGP PUBLIC KEY BLOCK ... '
	context.vars['publicKey'] = key.publicKeyArmored;
    });
    return next(); // MUST be called for the scenario to continue
}

function generateUser(requestParams, context, ee, next) {
    context.vars['username'] =  crypto
        .randomBytes(Math.ceil(20 / 2))
        .toString('hex') // convert to hexadecimal format
        .slice(0, 20);
    
    var options = {
	userIds: [{ name:'Jon Smith', email:'jon@example.com' }], // multiple user IDs
	curve: "ed25519",                                         // ECC curve name
	passphrase: 'super long and hard to guess secret'         // protects the private key
    };
    
    openpgp.generateKey(options).then(function(key) {
	var privkey = key.privateKeyArmored; // '-----BEGIN PGP PRIVATE KEY BLOCK ... '
	var pubkey = key.publicKeyArmored;   // '-----BEGIN PGP PUBLIC KEY BLOCK ... '
	var revocationCertificate = key.revocationCertificate; // '-----BEGIN PGP PUBLIC KEY BLOCK ... '
	context.vars['publicKey'] = key.publicKeyArmored;
	//console.log("Username: " + context.vars['username'] + "\nPublicKey: " + context.vars['publicKey']);
	return next();
    });
}
