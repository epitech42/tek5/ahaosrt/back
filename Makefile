DC=docker-compose

all:start

init:
	@echo "INIT PROJECT"
	@echo "Copying .env.dist in .env"
	@cp .env.dist .env
	@sed -i "s@secret@`tr -dc A-Za-z0-9_ < /dev/urandom | head -c 4096 | xargs`@g" .env
	@echo ".env: \n"
	@cat .env
	@echo "\n"

dev:
	@echo "Launch attached project and build\n"
	$(DC) up --build

build:
	@echo "build\n"
	$(DC) build
start:
	@echo "Launch dettached projet and build\n"
	$(DC) up -d --build
stop:
	$(DC) stop
clean:
	$(DC) down
tree:
	@tree | sed 's/├/\+/g; s/─/-/g; s/└/\\/g'
logs:
	$(DC) -f docker-compose.yml logs -f api
test:
	@echo "Launch test\n"
	$(DC) -f docker-compose.test.yml up -d --build

test_clean:
	@echo "Remove test\n"
	$(DC) -f docker-compose.test.yml down

test_logs:
	@echo "Attach logs\n"
	$(DC) -f docker-compose.test.yml logs -f unitary-test

test_re: test_clean test

linter:
	$(DC) -f docker-compose.yml exec api /usr/src/app/node_modules/eslint/bin/eslint.js --fix src/

migration:
	$(DC) -f docker-compose.yml exec api /usr/src/app/node_modules/mongoose-migrate-2/bin/migrate -c src/

loadtest:
	@echo "Launch loadtest\n"
	$(DC) -f docker-compose.loadtest.yml up -d --build

loadtest_clean:
	@echo "Remove loadtest\n"
	$(DC) -f docker-compose.loadtest.yml down

loadtest_logs:
	@echo "Attach logs\n"
	$(DC) -f docker-compose.loadtest.yml logs -f node-load-test

loadtest_re: loadtest_clean loadtest

install_dc:
	curl https://get.docker.com | sh -

install_dcc:
	COMPOSE_VERSION=$(curl -s https://api.github.com/repos/docker/compose/releases/latest | grep 'tag_name' | cut -d\" -f4)
	sh -c "curl -L https://github.com/docker/compose/releases/download/${COMPOSE_VERSION}/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose"
	chmod +x /usr/local/bin/docker-compose
	sh -c "curl -L https://raw.githubusercontent.com/docker/compose/${COMPOSE_VERSION}/contrib/completion/bash/docker-compose > /etc/bash_completion.d/docker-compose"

	docker-compose -v

.PHONY: all test test_clean loadtest loadtest_clean
